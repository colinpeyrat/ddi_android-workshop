package ddi.gobelins.mynotes.add;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.mapbox.mapboxsdk.location.LocationServices;

import ddi.gobelins.mynotes.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class NoteAddFragment extends Fragment {


    private static final String TAG = NoteAddFragment.class.getName();
    private TextView noteAddTitle;
    private TextView noteAddDesc;
    private TextView noteAddSubmit;
    private TextView noteAddDate;
    private NoteAddListener listener;
    private LocationServices locationServices;

    public NoteAddFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_note_add, container, false);

        String date = getArguments().getString(NoteAddActivity.EXTRA_DATE);

        // get all needed attributes from view
        bindView(view);

        // set notes values to editText
        setValues(date);

        // create listener for "modifier" button
        initListener();

        return view;
    }

    private void setValues(String date) {
        noteAddDate.setText(date);
    }

    private void initListener() {
        noteAddSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick");
                String title = noteAddTitle.getText().toString();
                String desc = noteAddDesc.getText().toString();
                String date = noteAddDate.getText().toString();
                listener.onSubmitClicked(title, desc, date);
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof NoteAddListener) {
            listener = (NoteAddListener) context;
        } else {
            throw new ClassCastException(context.toString()
                    + " must implement MyListFragment.OnItemSelectedListener");
        }
    }

    private void bindView(View view) {
        noteAddTitle = (TextView) view.findViewById(R.id.note_add_title);
        noteAddDesc = (TextView) view.findViewById(R.id.note_add_description);
        noteAddDate = (TextView) view.findViewById(R.id.note_add_date);
        noteAddSubmit = (Button) view.findViewById(R.id.note_add_submit);
    }

    public interface NoteAddListener {
        void onSubmitClicked(String title, String desc, String date);
    }

}
