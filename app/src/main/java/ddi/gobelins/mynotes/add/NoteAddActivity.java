package ddi.gobelins.mynotes.add;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.mapbox.mapboxsdk.MapboxAccountManager;
import com.mapbox.mapboxsdk.location.LocationListener;
import com.mapbox.mapboxsdk.location.LocationServices;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import ddi.gobelins.mynotes.R;
import ddi.gobelins.mynotes.datas.NoteManager;
import ddi.gobelins.mynotes.home.HomeActivity;

public class NoteAddActivity extends AppCompatActivity implements NoteAddFragment.NoteAddListener {

    public static final String EXTRA_DATE = "NOTE_ADD_DATE";
    private static final int PERMISSIONS_LOCATION = 0;
    private static final String TAG = NoteAddActivity.class.getName();
    private NoteAddFragment addFrag;
    private LocationServices locationServices;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_add);

        addFrag = new NoteAddFragment();

        String date = getDate();

        initBundle(date);

        MapboxAccountManager.start(this, getString(R.string.access_token));

        locationServices = LocationServices.getLocationServices(this);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.add_fragment_container, addFrag);
        ft.commit();
    }

    private void initBundle(String date) {
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_DATE, date);

        addFrag.setArguments(bundle);
    }

    private String getDate() {
        return new SimpleDateFormat("dd/MM/yyyy", Locale.FRENCH).format(new Date());
    }

    @Override
    public void onSubmitClicked(String title, String desc, String date) {
        Log.d(TAG, "onSubmitClicked");

        // Not working, see README for more informations
        //toggleGPS();

        if (!title.isEmpty() && !desc.isEmpty() && !date.isEmpty()) {
            addNotes(title, desc, date);
        } else {
            displayFormError();
        }
    }

    private void addNotes(String title, String desc, String date) {
        NoteManager.getInstance().addNote(title, desc, date);
        Toast.makeText(getApplicationContext(), "Votre note \"" + title + "\" à bien été ajouté.", Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
    }

    private void displayFormError() {
        Toast.makeText(getApplicationContext(), "Tous les champs sont obligatoires.", Toast.LENGTH_SHORT).show();
    }

    private void toggleGPS() {
        if (!locationServices.areLocationPermissionsGranted()) {
            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSIONS_LOCATION);
        } else {
            enableLocation(true);
        }
    }

    private void enableLocation(boolean enabled) {
        if (enabled) {
            // If we have the last location of the user, we can move the camera to that position.
            Location lastLocation = locationServices.getLastLocation();

            locationServices.addLocationListener(new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    if (location != null) {
                        // Should log current location but never get called because of permissions (view README)
                        Log.d(TAG, "onLocationChanged" + location.toString());
                    }
                }
            });
        }
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_LOCATION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                enableLocation(true);
            }
        }
    }
}
