package ddi.gobelins.mynotes.list;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ddi.gobelins.mynotes.R;
import ddi.gobelins.mynotes.datas.Note;


public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.ViewHolder> {

    private static NotesAdapterListener noteListener;
    private final List<Note> notes;

    public NotesAdapter(List<Note> notes) {
        this.notes = notes;
    }

    public void setNoteListener(NotesAdapterListener noteListener) {
        NotesAdapter.noteListener = noteListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.list_notes_item, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Note note = notes.get(position);

        String title = note.getTitle();
        String description = note.getDescription();
        String date = note.getDate();

        holder.setTitle(title);
        holder.setDescription(description);
        holder.setDate(date);
    }

    @Override
    public int getItemCount() {
        return notes.size();
    }

    public interface NotesAdapterListener {
        void onItemClick(int position);

        void onItemLongClick(int position, View view);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView noteTitle;
        private final TextView noteDescription;
        private final TextView noteDate;

        public ViewHolder(View itemView) {
            super(itemView);

            initClickListener(itemView);

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    noteListener.onItemLongClick(getAdapterPosition(), view);
                    return false;
                }
            });

            noteTitle = (TextView) itemView.findViewById(R.id.list_notes_item_title);
            noteDescription = (TextView) itemView.findViewById(R.id.list_notes_item_description);
            noteDate = (TextView) itemView.findViewById(R.id.list_notes_item_date);
        }

        private void initClickListener(View itemView) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    noteListener.onItemClick(getAdapterPosition());
                }
            });
        }

        public void setTitle(String title) {
            noteTitle.setText(title);
        }

        public void setDescription(String description) {
            noteDescription.setText(description);
        }

        public void setDate(String date) {
            noteDate.setText(date);
        }
    }
}
