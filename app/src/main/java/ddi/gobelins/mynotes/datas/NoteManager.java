package ddi.gobelins.mynotes.datas;


import java.util.List;

public class NoteManager {
    private static NoteManager instance = new NoteManager();
    // parce que c'est plus propre de manipuler un objets Notes plutot qu'un array de notes
    // En final on est sur que c'est immuable
    private Notes notes = new Notes();

    private NoteManager() {
        getNotes();
    }

    public static NoteManager getInstance() {
        return instance;
    }

    public List<Note> getNotes() {
        return notes.getNotes();
    }

    public void addNote(String title, String description, String date) {
        notes.addNote(new Note(title, description, date));
    }

    public void addNote(int position, String title, String description, String date) {
        notes.addNote(position, new Note(title, description, date));
    }

    public Note getNote(int id) {
        return notes.getNote(id);
    }

    public void updateNote(int position, String title, String desc, String date) {
        notes.updateNote(position, title, desc, date);
    }

    public void deleteNote(int position) {
        notes.deleteNote(position);
    }

    public void deleteAll() {
        notes.deleteAll();
    }
}
