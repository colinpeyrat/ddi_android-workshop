package ddi.gobelins.mynotes.datas;


import java.util.List;

public class Notes {
    private List<Note> noteList;

    public Notes() {
    }

    public void addNote(Note note) {
        note.save();
        noteList.add(note);
    }

    public void addNote(int position, Note note) {
        note.save();
        noteList.add(position, note);
    }

    public List<Note> getNotes() {
        noteList = Note.listAll(Note.class);
        return noteList;
    }

    public Note getNote(int id) {
        return noteList.get(id);
    }

    public void updateNote(int position, String title, String desc, String date) {
        Note note = noteList.get(position);
        note.setTitle(title);
        note.setDescription(desc);
        note.setDate(date);
        note.save();
    }

    public void deleteNote(int position) {
        Note note = noteList.get(position);
        noteList.remove(position);
        note.delete();
    }

    public void deleteAll() {
        noteList.clear();
        Note.deleteAll(Note.class);
    }
}
