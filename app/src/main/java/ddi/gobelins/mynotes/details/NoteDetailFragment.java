package ddi.gobelins.mynotes.details;


import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mapbox.mapboxsdk.MapboxAccountManager;
import com.mapbox.mapboxsdk.annotations.MarkerViewOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.constants.Style;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;

import ddi.gobelins.mynotes.R;
import ddi.gobelins.mynotes.datas.Note;
import ddi.gobelins.mynotes.datas.NoteManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class NoteDetailFragment extends Fragment {


    private static final String TAG = NoteDetailFragment.class.getName();
    private TextView noteDetailTitle;
    private TextView noteDetailDescription;
    private TextView noteDetailDate;
    private FloatingActionButton fabEdit;
    private NoteDetailListener listener;
    private MapView mapView;

    public NoteDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_note_details, container, false);

        MapboxAccountManager.start(getContext(), getString(R.string.access_token));

        noteDetailTitle = (TextView) view.findViewById(R.id.note_detail_title);
        noteDetailDescription = (TextView) view.findViewById(R.id.note_detail_description);
        noteDetailDate = (TextView) view.findViewById(R.id.note_detail_date);
        fabEdit = (FloatingActionButton) view.findViewById(R.id.fab_edit);

        initMap(savedInstanceState, view);

        int position = getArguments().getInt(NoteDetailActivity.EXTRA_POSTION);

        Log.d(TAG, "item position clicked : " + String.valueOf(position));

        // set notes values to textView
        bindValues(position);

        // create listener for fab
        initClickListener();

        return view;
    }

    private void initMap(Bundle savedInstanceState, View view) {
        mapView = (MapView) view.findViewById(R.id.mapview);

        mapView.onCreate(savedInstanceState);

        mapView.setStyleUrl(Style.MAPBOX_STREETS);

        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {
                LatLng position = new LatLng(45.8950739, 6.1263277, 17);

                MarkerViewOptions marker = new MarkerViewOptions()
                        .position(position);

                mapboxMap.setCameraPosition(new CameraPosition.Builder()
                        .zoom(12)
                        .target(position)
                        .build());

                mapboxMap.addMarker(marker);
            }
        });
    }

    public void bindValues(int position) {
        Note note = NoteManager.getInstance().getNote(position);
        initView(note);
    }

    private void initView(Note note) {
        noteDetailTitle.setText(note.getTitle());
        noteDetailDescription.setText(note.getDescription());
        noteDetailDate.setText(note.getDate());
    }

    private void initClickListener() {
        fabEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onFabClicked();
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof NoteDetailListener) {
            listener = (NoteDetailListener) context;
        } else {
            throw new ClassCastException(context.toString()
                    + " must implement MyListFragment.OnItemSelectedListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    public interface NoteDetailListener {
        void onFabClicked();
    }
}
