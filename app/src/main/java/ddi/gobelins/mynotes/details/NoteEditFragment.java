package ddi.gobelins.mynotes.details;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import ddi.gobelins.mynotes.R;
import ddi.gobelins.mynotes.datas.Note;
import ddi.gobelins.mynotes.datas.NoteManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class NoteEditFragment extends Fragment {

    private static final String TAG = NoteDetailActivity.class.getName();
    private TextView noteEditTitle;
    private TextView noteEditDesc;
    private TextView noteEditDate;
    private Button noteEditButton;
    private NoteEditListener listener;
    private int position;

    public NoteEditFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_note_edit, container, false);

        position = getArguments().getInt(NoteDetailActivity.EXTRA_POSTION);

        // get the note from position received from bundle
        Note note = NoteManager.getInstance().getNote(position);

        // get all needed attributes from view
        bindView(view);

        // set notes values to editText
        setValues(note);

        noteEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title = noteEditTitle.getText().toString();
                String desc = noteEditDesc.getText().toString();
                String date = noteEditDate.getText().toString();
                listener.onSubmitClicked(position, title, desc, date);
            }
        });

        Log.d(TAG, "item position to update : " + String.valueOf(position));

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof NoteEditListener) {
            listener = (NoteEditListener) context;
        } else {
            throw new ClassCastException(context.toString()
                    + " must implement MyListFragment.OnItemSelectedListener");
        }
    }

    private void setValues(Note note) {
        noteEditTitle.setText(note.getTitle());
        noteEditDesc.setText(note.getDescription());
        noteEditDate.setText(note.getDate());
    }

    private void bindView(View view) {
        noteEditTitle = (TextView) view.findViewById(R.id.note_edit_title);
        noteEditDesc = (TextView) view.findViewById(R.id.note_edit_description);
        noteEditDate = (TextView) view.findViewById(R.id.note_edit_date);
        noteEditButton = (Button) view.findViewById(R.id.note_edit_button);
    }

    public interface NoteEditListener {
        void onSubmitClicked(int position, String title, String desc, String date);
    }
}
