package ddi.gobelins.mynotes.details;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import ddi.gobelins.mynotes.R;
import ddi.gobelins.mynotes.datas.NoteManager;
import ddi.gobelins.mynotes.home.HomeActivity;

public class NoteDetailActivity extends AppCompatActivity implements NoteDetailFragment.NoteDetailListener, NoteEditFragment.NoteEditListener {

    public static final String EXTRA_POSTION = "NOTE_DETAIL_POSITION";
    private static final String TAG = NoteDetailActivity.class.getName();
    private android.support.v4.app.Fragment detailFrag;
    private android.support.v4.app.Fragment editFrag;
    private int position;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_detail);

        Intent intent = getIntent();
        position = intent.getIntExtra(HomeActivity.EXTRA_POSITION, 0);

        // Position on recyclerView start at 0, and ID of Sugar ORM start at 1.
        // I increment position to fetch both recyclerView and ORM
        //position++;

        Log.d(TAG, "position item clicked : " + String.valueOf(position));

        // create the fragment for this activity
        detailFrag = new NoteDetailFragment();
        editFrag = new NoteEditFragment();

        // send position current item to fragment
        initBundle(position);

        // display detail of notes when the activity is launched
        displayFragmentDetail();
    }

    private void initBundle(int position) {
        Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_POSTION, position);

        detailFrag.setArguments(bundle);
        editFrag.setArguments(bundle);
    }

    protected void displayFragmentDetail() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_container, detailFrag);
        ft.commit();
    }

    protected void displayFragmentEdit() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.addToBackStack(editFrag.getClass().getName());
        ft.replace(R.id.fragment_container, editFrag);
        ft.commit();
    }

    @Override
    public void onFabClicked() {
        Log.d(TAG, "DetailFragment fab clicked");
        displayFragmentEdit();
    }

    @Override
    public void onSubmitClicked(int position, String title, String desc, String date) {
        Log.d(TAG, "EditFragment Submit clicked " + String.valueOf(position));

        if (!title.isEmpty() && !desc.isEmpty() && !date.isEmpty()) {
            addNote(position, title, desc, date);
        } else {
            displayFormError();
        }

    }

    private void addNote(int position, String title, String desc, String date) {
        NoteManager.getInstance().updateNote(position, title, desc, date);
        Toast.makeText(getApplicationContext(), "Votre note à bien été mise à jour.", Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
    }

    private void displayFormError() {
        Toast.makeText(getApplicationContext(), "Tous les champs sont obligatoires.", Toast.LENGTH_SHORT).show();
    }

    private void popBackStack() {
        FragmentManager fm = getSupportFragmentManager();
        fm.popBackStack();
    }
}