package ddi.gobelins.mynotes.splash;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import ddi.gobelins.mynotes.home.HomeActivity;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // This activity has the SplashTheme as theme
        // I create a drawable layout, and set is a background of the SplashTheme
        // Launch directly the HomeActivity, wait time depends to speed of the phone
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        finish();
    }
}
