package ddi.gobelins.mynotes.home;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ddi.gobelins.mynotes.R;
import ddi.gobelins.mynotes.datas.Note;
import ddi.gobelins.mynotes.datas.NoteManager;
import ddi.gobelins.mynotes.list.NotesAdapter;

/**
 * A placeholder fragment containing a simple view.
 */
public class ListNotesActivityFragment extends Fragment {

    private static final String TAG = ListNotesActivityFragment.class.getName();
    private RecyclerView recyclerView;
    private NotesAdapter notesAdapter;
    private List<Note> notes;

    public ListNotesActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        initList(view);

        initNotesAdapter();

        initClickListener();

        return view;
    }

    private void initClickListener() {
        notesAdapter.setNoteListener((NotesAdapter.NotesAdapterListener) getActivity());
    }

    private void initNotesAdapter() {
        notes = NoteManager.getInstance().getNotes();
        notesAdapter = new NotesAdapter(notes);
        recyclerView.setAdapter(notesAdapter);
    }

    private void initList(View view) {
        recyclerView = (RecyclerView) view.findViewById(R.id.list_notes_recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
    }

    public void refreshList() {
        Log.d(TAG, "Refresh List");
        notesAdapter.notifyDataSetChanged();
    }

    public void notifyItemRemoved(int position) {
        notesAdapter.notifyItemRemoved(position);
    }

    public void notifyItemInserted(int position) {
        notesAdapter.notifyItemInserted(position);
    }
}