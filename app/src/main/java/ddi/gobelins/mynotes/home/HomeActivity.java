package ddi.gobelins.mynotes.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import ddi.gobelins.mynotes.R;
import ddi.gobelins.mynotes.add.NoteAddActivity;
import ddi.gobelins.mynotes.datas.Note;
import ddi.gobelins.mynotes.datas.NoteManager;
import ddi.gobelins.mynotes.details.NoteDetailActivity;
import ddi.gobelins.mynotes.list.NotesAdapter;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, NotesAdapter.NotesAdapterListener {

    public static final String EXTRA_POSITION = "NOTE_POSITION";
    private static final String TAG = HomeActivity.class.getName();
    public ListNotesActivityFragment fragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        fragment = (ListNotesActivityFragment) getSupportFragmentManager().findFragmentById(R.id.fragment);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "fab add clicked");
                Intent intent = new Intent(view.getContext(), NoteAddActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_delete_all) {
            deleteAllNotes();
        }

        return super.onOptionsItemSelected(item);
    }

    private void deleteAllNotes() {
        NoteManager.getInstance().deleteAll();
        fragment.refreshList();
        Toast.makeText(getApplicationContext(), "Toutes les notes ont bien été supprimées.", Toast.LENGTH_SHORT).show();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_add) {
            Intent intent = new Intent(this, NoteAddActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_action_delete_all) {
            deleteAllNotes();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onItemClick(int position) {
        // transition swipeFromRight
        Log.d(TAG, "click on item with position : " + position);
        Intent intent = new Intent(this, NoteDetailActivity.class);
        intent.putExtra(EXTRA_POSITION, position);
        startActivity(intent);
    }

    @Override
    public void onItemLongClick(final int position, View view) {
        Log.d(TAG, "long click on item with position : " + position);
        fragment.notifyItemRemoved(position);
        final Note noteToDelete = NoteManager.getInstance().getNote(position);
        Snackbar snackbarRemove = Snackbar.make(view, "La note a été supprimé", Snackbar.LENGTH_LONG)
                .setAction("Annuler", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.d(TAG, "Note to restore " + noteToDelete.getTitle());
                        NoteManager.getInstance().addNote(position, noteToDelete.getTitle(), noteToDelete.getDescription(), noteToDelete.getDate());
                        fragment.notifyItemInserted(position);
                    }
                });
        snackbarRemove.show();
        NoteManager.getInstance().deleteNote(position);
    }
}
