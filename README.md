# Smeio

## Difficultés rencontrées :

- J'ai eu du mal au début pour gérer la différence entre la position et id de l'ORM pour une note dans la liste. au final je suis toujours passé par la position (moins de soucis au niveau de la suppression.)
- J'ai mis du temps avant d'arriver à saisir complètement la logique pour écouter les cliques d'un fragment depuis une activité
- Appeler le refresh de list depuis une activité qui n'a pas le fragment de la liste. (.Au final, je l'ai gé ré avec un `finish()`.)
- Mapbox qui affichait une carte noire, car les styles définit dans le XML n'était pas pris en compte ( obligé de définir directement dans le je code Java ).
- j'ai suivi le guide, mais le requestPermissions n'ouvre aucune fenêtre de dialogue, du coup la méthode `onRequestPermissionsResult()` n'est jamais appelé alors que les permissions sont bien dans le manifest

## En cas d'erreur de compilation :

1. Si vous êtes sur Android Studio 2.0 ou plus: enlever l'instant run.
2. Incrémenter la META-DATA `VERSION` dans le fichier AndroidManifest.xml
